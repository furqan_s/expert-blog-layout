<?php
/*

Plugin Name:     Expert Blog Layout
Plugin URI:      http://domain.com
Version:         1.0.0
Description:     Customize your blog in more unique and creative ways.
Author:          Syed Furqan Ali
Author URI:      http://domain.com
License:         GPL2

Expert Blog Layout is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Expert Blog Layout is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Expert Blog Layout. If not, see {URI to Plugin License}.

*/

// Exit if accessed directly
if ( ! defined( "ABSPATH" ) ) exit; 

// Include Shortcode file
include_once('includes/shortcode.php');

// Plugin Menu in wordpress 
add_action('admin_menu', 'blog_plugin_menu');
function blog_plugin_menu(){
	add_menu_page('EBL Shortcode Page' , 'EBL Shortcode', 'administrator', 'blog_layout', 'blog_layout_function', 'dashicons-format-gallery', 30);
}

// adding a function to add_menu_page method
function blog_layout_function(){
	include_once('includes/expert-blog-admin.php');
}

// adding CSS for admin pages only
add_action('ebl_scripts','ebl_admin_scripts');
function ebl_admin_scripts(){
	wp_enqueue_script( 'custom', plugins_url() .'/expert-blog-layout/admin/js/custom.js', array(), '1.0.0', 'true' );
	wp_enqueue_style('ebl-admin-grid-styles', plugins_url() .'/expert-blog-layout/admin/css/grid.css', array());
	wp_enqueue_style('ebl-admin-styles', plugins_url() .'/expert-blog-layout/admin/css/admin.css', array());
}

// adding css for public pages only
add_action( 'wp_enqueue_scripts', 'blog_enqueue_scripts' );
function blog_enqueue_scripts() {
	wp_enqueue_style('expert-blog-layout-grid-css', plugins_url() .'/expert-blog-layout/public/css/grid.css', array());
	wp_enqueue_style('blog-style', plugins_url() .'/expert-blog-layout/public/css/blog.css', array());   
}


?>