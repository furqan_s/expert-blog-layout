/*** Making shortcode based on user selection and showing it ****/
jQuery('.generate-shortcode button').click(function() {
    var blogstyle = jQuery('input[type="radio"][name="blog-style-selection"]:checked').val();
    jQuery('#ebl-blog-shortcode').html('[ebl_blog blog-style="'+blogstyle+'"]');
	jQuery('.ebl-blog-shortcode ').show();
});

/*** Code to copy shortcode to clipboard ****/
function copyToClipboard(element) {
    var $temp = jQuery("<input>");
    jQuery("body").append($temp);
    $temp.val(jQuery(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    alert('Text Copied');
}