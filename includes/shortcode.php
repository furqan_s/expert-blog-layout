<?php



if ( ! defined( "ABSPATH" ) ) exit; // Exit if accessed directly


function eblblog($atts = array(), $content = ''){

  global $post;

  // setting default value for shortcode
  $atts = shortcode_atts( array(
    'blog-style' =>'style-1',
  ),  $atts);

  // assigning values to args array
  $args = array(
   'post_type'   => 'post',
   'post_status' => 'publish',  
  );

  // concatenating html code into php variable with dynamic values 
  $content .="

    <div class='blog-section'>

    <div class='ebl-container'>

      <div class='ebl-row'>

      ";
      //passing args to wp query function
      $blog_item = new WP_Query( $args );

      // checking if wp query has returned posts
      if($blog_item->have_posts() ) :

      // loop through till it has posts
      while( $blog_item->have_posts() ) :

      $blog_item->the_post();
     
      // getting different attributes from post object
      $blog_id = get_the_id();
      $blog_title = get_the_title();
      $blog_desc = get_the_excerpt();
      $blog_image = get_the_post_thumbnail();
      $blog_link = get_the_permalink();
      $blog_day = get_the_date('j');
      $blog_month = get_the_date('M');

      // if blog image is empty use a default image
      if(empty($blog_image)){
        $blog_image = '<img src="'.plugins_url().'/expert-blog-layout/public/images/default-image.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="">';
      }

      // checking the blog layout and including file as per that
      if($atts['blog-style']=="style-1"){
            include_once('layout-1.php');
      }
  
      // ending while loop
      endwhile;

      else :
      // if there is no post showing the below message
      echo '<h3>No Post Found.<h3>';

      endif;


    // concatenating remaining html tags
    $content .="</div>

    </div> 

    </div>";

  // returning variable after concatenation
  return $content;



  }

add_shortcode( 'ebl_blog', 'eblblog' );

?>