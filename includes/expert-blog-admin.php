<?php 

// Executing admin related scripts
do_action('ebl_scripts');

?>

<div class="ebl-container-fluid">

    <div class="ebl-header">
        <div class="ebl-row">
            <div class="ebl-col-md-12">
                <h1>Expert Blog Layout </h1>
            </div>
        </div>
    </div> <!-- Ebl Header end -->

    <div class="blog-selection">
        <h2>Select your Blog Style</h2>
        <div class="ebl-row">
            <ul>
                <li>
                    <input type="radio" id="blog-style-admin" value="style-1" name="blog-style-selection" checked="checked">
                    <label>
                        <img src="<?php echo plugins_url() ?>/expert-blog-layout/admin/images/blog-style-1.png"
                        alt="" />
                        <p>Blog Style 1</p>
                    </label>
                </li>
            </ul>
        </div>
    </div> <!-- Blog Section end -->

    <div class="generate-shortcode">
        <button>Generate Shortcode</button>
    </div> <!-- Generate shortcode end -->

    <div class="ebl-blog-shortcode">
        <span id="ebl-blog-shortcode"> </span>
        <span class="dashicons dashicons-admin-page copy-icon" id="copy-btn" onclick="copyToClipboard('#ebl-blog-shortcode')"></span>
        <p class="copy-disclaimer">Copy the above shortcode and paste on any post/page.</p>
    </div> <!-- Shortcode end -->

</div> <!-- main continer ends -->




